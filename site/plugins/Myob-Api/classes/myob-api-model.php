<?php
if ( !class_exists( 'Aspk_Myob_Api_Model' )){

	class Aspk_Myob_Api_Model{
		
		function __construct(){
			
		}
		
		function install_model(){
			global $wpdb;
				
			$sql = "CREATE TABLE IF NOT EXISTS  `".$wpdb->prefix."aspk_qty_updates` ( 
				`itemid` VARCHAR( 100 ) NOT NULL  PRIMARY KEY ,
				`dt` bigint  NOT NULL ,
				`qtyupdated` bigint NULL 
				) ENGINE = InnoDB;";
			$wpdb->query($sql);
			
		}
		
		function get_orderid_by_date($post_date){
			global $wpdb;
			
			$sql = "SELECT `ID` FROM {$wpdb->prefix}posts WHERE `post_type`= 'shop_order' AND post_date >='{$post_date}'";
			return $wpdb->get_col($sql);
			
		}
		
		function get_orderid($post_id){
			global $wpdb;
			
			$sql = "SELECT `ID` FROM {$wpdb->prefix}posts WHERE `post_type`= 'shop_order' AND ID >'{$post_id}'";
			return $wpdb->get_col($sql);
			
		}
		
		function get_posted_date($post_id){
			global $wpdb;
			
			$sql = "SELECT `post_date` FROM {$wpdb->prefix}posts WHERE `post_type`= 'shop_order' AND ID >='{$post_id}'";
			$dt_time = $wpdb->get_var($sql);

			$tm = strtotime($dt_time);
			return date('Y-m-d', $tm);
			
		}
		
	} //class ends
}//if class ends