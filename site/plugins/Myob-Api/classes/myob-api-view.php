<?php
if ( !class_exists( 'Aspk_Myob_Api_View' )){

	class Aspk_Myob_Api_View{
		
		function __construct(){
			
		}
		
		function send_order(){
			$url = get_option( 'aspk_user_uri');
			$user_auth_name = get_option( 'aspk_user_name_auth');
			$user_api_key = get_option( 'aspk_user_api_key');
			$user_acces_token = get_option( 'aspk_user_access_key');
			
			$headers = array();
			$authorization = 'Basic '.$user_auth_name;
			$headers['Authorization'] = $authorization;
			$headers['x-myobapi-key'] = $user_api_key;
			$headers['x-myobapi-exotoken'] = $user_acces_token;
			$headers['Accept'] = 'application/json';
		
			$args = array(
				'method' => 'POST',
				'timeout' => 45,
				'redirection' => 5,
				'httpversion' => '1.0',
				'blocking' => true,
				'headers' => $headers,
				'body' => array(),
				'cookies' => array()
			);
			
			$response = wp_remote_post( $url, $args);
			
			$retarr = array();
			
			if ( is_wp_error( $response ) ) {
			   $error_message = $response->get_error_message();
			   $retarr['status'] = "error";
			   $retarr['err_msg'] = $error_message;
			   
			   //send to order notes with dt
			}else{
				
				if(stristr($response['body'], 'Error')){
					$retarr['status'] = "error";
					$retarr['err_msg'] = $response['body'];
					return $retarr;
				}
				
				$license_key = $response['body'];
			    $retarr['status'] = "ok";
			    $retarr['license_key'] = $license_key;
			}
			return $retarr;
		}
		
		function setting_api(){
			$uer_name = get_option( 'aspk_user_name');
			$user_pwd = get_option( 'aspk_user_pwd');
			$user_api = get_option( 'aspk_user_api_key');
			$user_acces_token = get_option( 'aspk_user_access_key');
			$user_uri = get_option( 'aspk_user_uri');
			$dropbox_username = get_option( 'aspk_username_dropbox');
			$dropbox_pswd = get_option( 'aspk_pswd_dropbox');
			?>
			<div class="tw-bs container">
				<div class="row">
					<div class="col-md-12" style="margin-top:1em;">
						<h2 style="text-align:center;">EXO API Setting</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" style="margin-top:1em;">
						<div style=""><b>CRON URL:</b>  <?php echo admin_url().'admin-ajax.php?action=aspk_dropbox'?></div>
					</div>
				</div>
				<form method="post" action="">
					<div class="row">
						<div class="col-md-2" style="margin-top:1em;"><b>User Name:</b></div>
						<div class="col-md-10" style="margin-top:1em;"><input style="width: 30em;" type="text" value="<?php echo $uer_name; ?>" name="aspk_username"></div>
					</div>
					<div class="row">
						<div class="col-md-2" style="margin-top:1em;"><b>Password</b></div>
						<div class="col-md-10" style="margin-top:1em;"><input style="width: 30em;" type="password" value="<?php echo $user_pwd; ?>" name="aspk_pswd"></div>
					</div>
					<div class="row">
						<div class="col-md-2" style="margin-top:1em;"><b>Dev Key</b></div>
						<div class="col-md-10" style="margin-top:1em;"><input style="width: 30em;" type="text" value="<?php echo $user_api; ?>" name="aspk_dev_key"></div>
					</div>
					<div class="row">
						<div class="col-md-2" style="margin-top:1em;"><b>Access Token</b></div>
						<div class="col-md-10" style="margin-top:1em;"><input style="width: 30em;" type="text" value="<?php echo $user_acces_token; ?>" name="aspk_access_token"></div>
					</div>
					<div class="row">
						<div class="col-md-2" style="margin-top:1em;"><b>URI</b></div>
						<div class="col-md-10" style="margin-top:1em;">
							<input style="width: 30em;" type="text" value="<?php echo $user_uri; ?>" name="aspk_uri">
						</div>
					</div>
					<div class="row">
						<div class="col-md-12" style="margin-top:1em;">
							<h3 style="text-align:center;">Dropbox Credentials</h3>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2" style="margin-top:1em;"><b>User id:</b></div>
						<div class="col-md-10" style="margin-top:1em;">
							<input style="width: 30em;" type="text" value="<?php echo $dropbox_username; ?>" name="aspk_drop_box_uid">
						</div>
					</div>
					<div class="row">
						<div class="col-md-2" style="margin-top:1em;"><b>Auth Token:</b></div>
						<div class="col-md-10" style="margin-top:1em;">
							<input style="width: 30em;" type="text" value="<?php echo $dropbox_pswd; ?>" name="aspk_auth_token">
						</div>
					</div>
					<div class="row">
						<div class="col-md-12" style="margin-top:1em;"><input type="submit" name="aspk_submit" value="Save Setting" class="btn btn-primary"></div>
					</div>
				</form>
			</div>
			<?php
		}
		
	} //class ends
}//if class ends