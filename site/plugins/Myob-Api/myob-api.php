<?php

/**
 * Plugin Name: Myob Api.
 * Plugin URI: 
 * Description: Myob Api.
 * Version: 2.0
 * Author: Agile Solutions PK.
 * Author URI: http://agilesolutionspk.com
 */
 
require_once(__DIR__ ."/classes/myob-api-view.php");
require_once(__DIR__ ."/classes/myob-api-model.php");
require_once(__DIR__ ."/Dropbox/autoload.php");

use \Dropbox as dbx;

function scoma($s){
	$s = str_replace(',','',$s);
	return $s;
}

 
if ( !class_exists('Aspk_Myob_Api_Solution')){
	class Aspk_Myob_Api_Solution{
		
		function post_save_and_update($product_id,$post){
			$post_type = $post->post_type;
			
			if( $post_type == 'product' ){
				
				$prod = new WC_Product($product_id);
				if($prod->is_virtual()) return;
				if(! $prod->managing_stock()) return;
				
				$tot = $this->get_qty_from_api($prod->get_sku());
				$prod->set_stock($tot);
			}
		}
		
		
		function admin_menu(){
			
			add_menu_page('EXO API', 'EXO API', 'manage_options', 'aspk_exo_api', array(&$this, 'aspk_setting_api'),plugins_url( 'Myob-Api/images/certificate2.gif' ) );
			
			
		}
		
		
		
		function cron(){
			global $wpdb;
			
			$sql = "Select ID from $wpdb->posts as p, $wpdb->postmeta as m where p.post_type = 'product' and p.post_status = 'publish' and m.post_id = p.ID and m.meta_key = '_manage_stock' and m.meta_value = 'yes' ";
			
			$postids = $wpdb->get_col($sql);
			
			if($postids){
				
				foreach($postids as $id){
					
					$prod = new WC_Product($id);
					if($prod->is_virtual()) return;
					if(! $prod->managing_stock()) return;
					
					$tot = $this->get_qty_from_api($prod->get_sku());
					$prod->set_stock($tot);
					
				}
				
			}
			
		}
		
		
		function aspk_setting_api(){
			$aspk_api_v = new Aspk_Myob_Api_View();
			
			if(isset($_POST['aspk_submit'])){
				$u_name = $_POST['aspk_username'];
				$pwd = $_POST['aspk_pswd'];
				$api_key = $_POST['aspk_dev_key'];
				$access_token = $_POST['aspk_access_token'];
				$uri = $_POST['aspk_uri'];
				$drop_box = $_POST['aspk_drop_box_uid'];
				$drop_box_pswd = $_POST['aspk_auth_token'];
				$username = base64_encode($u_name.':'.$pwd);
				
				update_option( 'aspk_user_name', $u_name );
				update_option( 'aspk_user_name_auth', $username );
				update_option( 'aspk_user_pwd', $pwd );
				update_option( 'aspk_user_api_key', $api_key );
				update_option( 'aspk_user_access_key', $access_token );
				update_option( 'aspk_user_uri', $uri );
				update_option( 'aspk_username_dropbox', $drop_box );
				update_option( 'aspk_pswd_dropbox', $drop_box_pswd );
			}
			
			$aspk_api_v->setting_api();
			
		}
		
		function order_status_changed($order_id ){
			global $woocommerce;
			
			$aspk_api_v = new Aspk_Myob_Api_View();
			$order = new WC_Order($order_id);
			$total = $order->get_total();
			$user_id = $order->user_id;
			$items = $order->get_items();
			
			$f_name = get_user_meta( $user_id, 'shipping_first_name', true );
			$l_name = get_user_meta( $user_id, 'shipping_last_name', true );
			

			$api_user_id = $this->get_user_from_api($f_name, $l_name);
			
			
			
			$user_name = $f_name.' '.$l_name;
			$user_email = get_user_meta( $user_id, 'shipping_email', true );
			$user_zip = get_user_meta( $user_id, 'shipping_postcode', true );
			$user_phone = get_user_meta( $user_id, 'shipping_phone', true );
			$company_name = get_user_meta( $user_id, 'shipping_company', true );
			
			$deliveryaddress = array();
			$deliveryaddress['line1'] = get_user_meta( $user_id, 'shipping_address_1', true );
			$deliveryaddress['line2'] = get_user_meta( $user_id, 'shipping_address_2', true );
			$deliveryaddress['line3'] = get_user_meta( $user_id, 'shipping_city', true );
			$deliveryaddress['line4'] = get_user_meta( $user_id, 'shipping_state', true );
			$deliveryaddress['line5'] = get_user_meta( $user_id, 'shipping_country', true );
			$deliveryaddress['line6'] =	'';
				
			if($api_user_id == '0'){
				
				$api_user_id = $this->create_user_via_api($user_name, $user_email, $user_zip, $user_phone, $deliveryaddress, $company_name);
				
				$order->add_order_note("Created customer on EXO with id ".$api_user_id);
				
			}else{
				$order->add_order_note("Customer exists on EXO with id ".$api_user_id);
			}
			
			$api_order_id = $this->create_order_via_api($api_user_id, $order_id, $items, $total, $deliveryaddress);
			
		}
		
		function create_order_via_api($debtorid, $order_id, $items, $total, $deliveryaddress){
			global $woocommerce;
			
			$line_items = array();
			
			foreach($items as $item){
				$line_item = array();
				
				$qty = $item['qty'];
				$product_id = $item['product_id'];
				$prod = new WC_Product($product_id);
				if($prod->is_virtual()){
					$stocktype = "LookupItem";
				}else{
					$stocktype = "PhysicalItem";
				}
				
				$line_item['stockcode'] = $prod->get_sku();
				$line_item['orderquantity'] = $qty;
				$line_item['unitprice'] = round( ($item['line_total'] / $item['qty'] ), 2);
				$line_item['taxratevalue'] = $item['line_tax'];
				$line_item['linetotal'] = $item['line_total'];
				$line_item['linetype'] = 0;
				$line_item['description'] = $prod->get_title();
				$line_item['stocktype'] = $stocktype;
				$line_item['duedate'] = date('Y-m-d');
				$line_item['IsOriginatedFromTemplate'] = 'false';
				$line_item['discount'] = 0;
				$line_item['IsPriceOverridden'] = 'false';
				$line_item['LocationId'] = 1;
				$line_item['TaxRateId'] = 10;
				$line_item['listprice'] = $line_item['unitprice'];
				$line_item['pricepolicyid'] = -1;
				$line_item['narrative'] = '';
				$line_item['BatchCode'] = '';
				//$line_item['BranchId'] = 0;
				
				$line_items[] = $line_item;
				
			}
			
			$order = new WC_Order($order_id);
			
			$shipping = $order->get_items( 'shipping' );
			$ship_method = array_pop($shipping);
			$ship_id = $ship_method['method_id'];
			
			
			$user_uri = get_option( 'aspk_user_uri');
			$url = $user_uri.'/salesorder/';
			
			$user_auth_name = get_option( 'aspk_user_name_auth');
			$user_api_key = get_option( 'aspk_user_api_key');
			$user_acces_token = get_option( 'aspk_user_access_key');
			
			$headers = array();
			$authorization = 'Basic '.$user_auth_name;
			$headers['Authorization'] = $authorization;
			$headers['x-myobapi-key'] = $user_api_key;
			$headers['x-myobapi-exotoken'] = $user_acces_token;
			$headers['Accept'] = 'application/json';
			$headers['Content-Type'] = 'application/json';
			
			
			$extra = array();
			
			if($ship_id == 'local_pickup'){
				
				$ship_via['key'] =  'X_SHIP_VIA';
				$ship_via['value'] =  'AUSTRALIA POST';
				
				$eparcel['key'] = 'X_COURIER_PRODUCT';
				$eparcel['value'] = 'X1';
				
				$extra[] = $ship_via;
				$extra[] = $eparcel;
				
			}else{
				
				$ship_via['key'] =  'X_SHIP_VIA';
				$ship_via['value'] =  'PICKUP';
				
				$extra[] = $ship_via;
				
			}
			
			$body = array(
				'debtorid' => $debtorid,
				'lines' => $line_items,
				'status' => 0,
				'ordertotal' => $total,
				'narrative' => home_url(),
				'allowcustomerordernumber' => 'true',
				'reference' => '',
				'instructions' => '',
				'finalisationdate' => '',
				'activationdate' => '',
				'BranchId' => 0,
				'DefaultLocationId' => -1,
				'ContactId' => -1,
				'SalesPersonId' => 61,
				'ExchangeRate' => 1,
				'OrderDate' => date('Y-m-d'),
				'duedate' => date('Y-m-d'),
				'DeliveryAddress' => $deliveryaddress,
				'extrafields' => $extra,
				'customerordernumber' => $order_id,
				'id' => 0,
				'currencyid' => 0
				
			);
						
			$args = array(
				'method' => 'POST',
				'timeout' => 45,
				'redirection' => 5,
				'httpversion' => '1.0',
				'blocking' => true,
				'headers' => $headers,
				'body' => json_encode($body),
				'cookies' => array()
			);
			
			
			
			$response = wp_remote_post( $url, $args);
			
			
			
			$body = $response['body'];
			$body_obj = json_decode($body);
			
			
			
			if ( is_wp_error( $response ) ) {
			   $error_message = $response->get_error_message();
			   $order->add_order_note("Error:".$error_message);
			   return '0';
			   
			}else{
				$ret = $response['response'];
				if($ret['code'] != '201'){
					$order->add_order_note("Error:".$ret['message'] .$body_obj->message  );
					return '0';
				}
				
				$order->add_order_note("Order created on EXO with id ".$body_obj->id);
				
				update_post_meta( $order_id, '_dropbox_order_id', $body_obj->id);
			}
			
		}
		
		
		
		
		function get_user_from_api($firstname, $lastname){
			
			$customer_name = $firstname.' '.$lastname;
			$user222 = urlencode ( $customer_name );
			
			$user_auth_name = get_option( 'aspk_user_name_auth');
			$user_api_key = get_option( 'aspk_user_api_key');
			$user_acces_token = get_option( 'aspk_user_access_key');
			$user_uri = get_option( 'aspk_user_uri');
			
			$url = $user_uri.'/debtor/search?q='.$user222; 
				
			$headers = array();
			$authorization = 'Basic '.$user_auth_name;
			$headers['Authorization'] = $authorization;
			$headers['x-myobapi-key'] = $user_api_key;
			$headers['x-myobapi-exotoken'] = $user_acces_token;
			$headers['Accept'] = 'application/json';
			
			$response = wp_remote_post( $url, array(
				'method' => 'GET',
				'timeout' => 45,
				'redirection' => 5,
				'httpversion' => '1.0',
				'blocking' => true,
				'headers' => $headers,
				'body' => array(),
				'cookies' => array()
				)
			);
			
			if ( is_wp_error( $response ) ) {
				//$error_message = $response->get_error_message();
				return '0';
			}else{
				$body = $response['body'];
				$body_obj = json_decode($body);
				foreach($body_obj as $obj){
					$acount_name = $obj->accountname;
					if( $customer_name == $acount_name ){
						return $obj->id;
					}
				}
				return '0';
			}
			
		}
		
		function create_user_via_api($user_name, $user_email, $user_zip, $user_phone, $deliveryaddress, $company_name = ''){
			$user_uri = get_option( 'aspk_user_uri');
			$url = $user_uri.'/debtor/';
			
			$user_auth_name = get_option( 'aspk_user_name_auth');
			$user_api_key = get_option( 'aspk_user_api_key');
			$user_acces_token = get_option( 'aspk_user_access_key');
			
			$headers = array();
			$authorization = 'Basic '.$user_auth_name;
			$headers['Authorization'] = $authorization;
			$headers['x-myobapi-key'] = $user_api_key;
			$headers['x-myobapi-exotoken'] = $user_acces_token;
			$headers['Accept'] = 'application/json';
			
			$body = array(
				'accountname' => $user_name,
				'email' => $user_email,
				'phone' => $user_phone,
				'postalcode' => $user_zip,
				'website' => home_url(),
				'defaultcompanyname' => $company_name,
				'deliveryaddress' => $deliveryaddress,
				'primarygroupid' => 0
			);
						
			$args = array(
				'method' => 'POST',
				'timeout' => 45,
				'redirection' => 5,
				'httpversion' => '1.0',
				'blocking' => true,
				'headers' => $headers,
				'body' => $body,
				'cookies' => array()
			);
			
			$response = wp_remote_post( $url, $args);
			$body = $response['body'];
			$body_obj = json_decode($body);
			
			if ( is_wp_error( $response ) ) {
			   $error_message = $response->get_error_message();
			   $retarr = '0';
			   return $retarr;
			   
			}else{
				
				if(stristr($response['body'], 'Error')){
					$retarr = '0';
					return $retarr;
				}
				
				$retarr = $body_obj->id;
			}
			return $retarr;
		}
		
		
		
		
		function fe_init(){

		}
		
		function __construct(){
			register_activation_hook( __FILE__, array(&$this, 'install') );
			register_deactivation_hook( __FILE__, array(&$this, 'uninstall') );
			add_action('wp_enqueue_scripts', array(&$this, 'wp_enqueue_scripts') );
			add_action('admin_enqueue_scripts', array(&$this, 'wp_enqueue_scripts') ); 
			add_action('init', array(&$this, 'fe_init'));
			add_action('admin_menu', array(&$this,'admin_menu'));	
			add_action( 'woocommerce_order_status_processing',array(&$this, 'order_status_changed'));
			add_action( 'save_post', array(&$this, 'post_save_and_update'),10,2 );
			add_action( 'aspk_task_hook', array(&$this, 'cron'));
			add_action('wp_ajax_aspk_dropbox', array(&$this,'ajax_process')); 
			add_action( 'wp_ajax_nopriv_aspk_dropbox',  array(&$this,'ajax_process' ));

		}
		
		function create_csv($oids){
			global $woocommerce;
			
			$model = new Aspk_Myob_Api_Model();
			
			$f = ABSPATH.'wp-content/uploads/'.'apicsv-'.time().'.csv';
			$file = fopen($f,"w");
			
			$hdr = $this->get_csv_header();
			$hdr = $hdr.PHP_EOL;;
			
			fwrite($file, $hdr);
			
			if(! empty($oids)){
				foreach ($oids as $oid){
					
					$order = new wc_order($oid);
					$items = $order->get_items();
					
					$user_id = $order->user_id;
					
					$shipping_postcode = get_post_meta( $oid, '_shipping_postcode', true );
					$city = get_post_meta( $oid, '_shipping_city', true );

					$f_name =  get_post_meta($oid,'_billing_first_name',true);
					$l_name = get_post_meta($oid,'_billing_last_name',true);
					
					$shipping_address_1 = get_post_meta($oid,'_shipping_address_1',true);
					$shipping_address_2 = get_post_meta($oid,'_shipping_address_2',true);
					$shipping_state = get_post_meta($oid,'_shipping_state',true);
					$country_code = get_post_meta($oid,'_shipping_country',true);

					
					$full_name = $f_name.' '.$l_name;
					
					foreach($items as $item){
						$line = "##NAME##,##POSTTIME##,##TRANSDATE##,947,4,,Paypal,,,46,##SUBTOTAL##,##TAXTOTAL##,Y,Paypal,0,0,0,N,,##DUEDATE##,,##DELIVADDR1##,##DELIVADDR2##,##DELIVADDR3##,##DELIVADDR4##,##CONTACT_SEQNO##,0,1,,,,,##ORD_REF##,,##SO_SEQNO##,,,##DELIVADDR5##,##DELIVADDR6##,,,0,##AMOUNT##,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,AUSTRALIA POST,,";
						
						$sub_tot = $item['line_subtotal'];
						$text_tot = $item['line_subtotal_tax'];
						$sub_total = round($sub_tot,2);
						$text_total = round($text_tot,2);
						$post_date = $model->get_posted_date($oid);
						$post_time = date('H:i:s', current_time( 'timestamp' ));
						$line = str_replace('##POSTTIME##',$post_time,$line);
						$line = str_replace('##TRANSDATE##',$post_date,$line);
						$line = str_replace('##NAME##',scoma($full_name),$line);
						$line = str_replace('##SUBTOTAL##',$sub_total,$line);
						$line = str_replace('##TAXTOTAL##',$text_total,$line);
						$line = str_replace('##DUEDATE##',$post_date,$line);
						$line = str_replace('##DELIVADDR1##',scoma($shipping_address_1),$line);
						$line = str_replace('##DELIVADDR2##',scoma($shipping_address_2),$line);
						$line = str_replace('##DELIVADDR3##',scoma($city),$line);
						$line = str_replace('##DELIVADDR4##',scoma($shipping_state),$line);
						$line = str_replace('##CONTACT_SEQNO##',$oid,$line);
						$line = str_replace('##ORD_REF##',$oid,$line);
						$line = str_replace('##SO_SEQNO##',$oid,$line);
						$line = str_replace('##AMOUNT##',$sub_total,$line);
						$line = str_replace('##DELIVADDR6##',scoma($country_code),$line);
						$line = str_replace('##DELIVADDR5##',$shipping_postcode,$line);
						$line = $line.PHP_EOL;

						fwrite($file, $line);
						
					}
					

				}
			}

			fclose($file);
			return $f;

		}
		
		
		function get_csv_header(){
			
			$hdr = "NAME,POSTTIME,TRANSDATE,ACCNO,TRANSTYPE,INVNO,REF1,REF2,REF3,SALESNO,SUBTOTAL,TAXTOTAL,TAXINC,ANALYSIS,ALLOCATEDBAL,ALLOCATED,ALLOCAGE,GLPOSTED,GLCODE,DUEDATE,BRANCH_ACCNO,DELIVADDR1,DELIVADDR2,DELIVADDR3,DELIVADDR4,CONTACT_SEQNO,CURRENCYNO,EXCHRATE,BATCHNO,SHIFTNO,GLSUBCODE,BRANCHNO,ORD_REF,DISPATCH_INFO,SO_SEQNO,TAXRATE,TAXRATE_NO,DELIVADDR5,DELIVADDR6,PREV_PERIOD_OPEN,TERMINAL_ID,DEPOSIT_STATUS,AMOUNT,UNREALISED_GAINS_GL_BATCH,TAXRETCODE,WEEK_NO,DDNO,RELEASEDAMT,SALES_ACCNO,FREIGHT_FREE,CONTRACT_HDR,BANKACCNO,BANKACCNAME,GLBATCHNO,NARRATIVE_SEQNO,TOAGEDBAL,EFTCAID,EFTSTAN,PAY_STATUS,PHYS_BRANCH,PHYS_STAFF,SESSION_ID,PREV_PERIOD_CLOSE,EFTAUTH,GATEWAYNO,SOURCEINV_SEQNO,TXID,PTNO,CUSTORDERNO,PERIOD_SEQNO,AGE_STAMP,AGE,TAXROUNDING,CAMPAIGN_WAVE_SEQNO,JOB_CONTRACT_BILLINGS_SEQNO,JOBNO,X_COMMENT,X_ICATEGORY,X_ICOMMENT,X_IDATE,X_IDESCRIPTION,X_IFILENAME,X_IFILEROWID,X_IIMPORTTYPE,X_IINVNO1,X_IITEMCODE,X_IMAPKEY1,X_IMAPKEY2,X_IMAPKEY3,X_IRECORDID,X_ITRANSDATE,X_SHIP_VIA,ORIGINAL_KEY";
			return $hdr;
			
		}
		
		function ajax_process(){
			
			try{
				
				$last_order_id =  get_option('aspk_last_csv_oid', 0);
				$oids = $this->get_orders($last_order_id);
				
				$csvfile  =  $this->create_csv($oids);

				$this->upload_to_dropbox($csvfile);
				
				$new_last_oid = array_pop($oids);
				
				if($new_last_oid){
					update_option( 'aspk_last_csv_oid', $new_last_oid);
				}
				unlink($csvfile);
				
			}catch(Exception $e){
				
				$admin_email = get_option( 'admin_email' );
				wp_mail( $admin_email, "Error on WC-EXO Processing", $e->getMessage());
				
			}
		}
		
		function upload_to_dropbox($csvfile){
			
			$accessToken = get_option( 'aspk_pswd_dropbox');
			$dbxClient = new dbx\Client($accessToken, "ASPK-WC-EXO");
			
			$f = fopen($csvfile, "rb");
			$result = $dbxClient->uploadFile("/changing_habits_payments_wc_exo_".date('Y-m-d',current_time(timestamp)).".csv", dbx\WriteMode::add(), $f);
			
			fclose($f);
			
			
		}
		
		
		function get_orders($last_order_id){
			$model = new Aspk_Myob_Api_Model();
			
			$current = new DateTime(date('Y-m-d',current_time(timestamp)));
			$current->sub(new DateInterval('P2D'));
			$prev_datetime = $current->format('Y-m-d');
			
			
			if($last_order_id == 0) {
				$x = $model->get_orderid_by_date($prev_datetime);
				return $x;
			}else{
				$x = $model->get_orderid($last_order_id);
				return $x;
			}
		}

		
		function get_qty_from_api($item_id){
			
			$user_auth_name = get_option( 'aspk_user_name_auth');
			$user_api_key = get_option( 'aspk_user_api_key');
			$user_acces_token = get_option( 'aspk_user_access_key');
			$user_uri = get_option( 'aspk_user_uri');
			
			$url = $user_uri.'/stockitem/'.$item_id;
				
			$headers = array();
			$authorization = 'Basic '.$user_auth_name;
			$headers['Authorization'] = $authorization;
			$headers['x-myobapi-key'] = $user_api_key;
			$headers['x-myobapi-exotoken'] = $user_acces_token;
			$headers['Accept'] = 'application/json';

			$response = wp_remote_post( $url, array(
				'method' => 'GET',
				'timeout' => 45,
				'redirection' => 5,
				'httpversion' => '1.0',
				'blocking' => true,
				'headers' => $headers,
				'body' => array(),
				'cookies' => array()
				)
			);
			
			if ( is_wp_error( $response ) ) {
				$error_message = $response->get_error_message();
				return '0';
			} else {
				$body = $response['body'];
				$body_obj = json_decode($body);
				$check_status = $body_obj->active;
				if($check_status == false){
					return '0';
				}
				$total = $body_obj->totalinstock;
				
				return $total;
			}
			
		}
		
		
		function install(){
			
			wp_clear_scheduled_hook('aspk_task_hook');
			wp_schedule_event( time(), 'hourly', 'aspk_task_hook' );
			
		}
		
		function uninstall(){
			
			wp_clear_scheduled_hook('aspk_task_hook');
			
		}
		
		function wp_enqueue_scripts(){
			wp_enqueue_script('jquery');
			wp_enqueue_script('js-fe-bootstrap',plugins_url('js/js-agile-bootstrap.js', __FILE__));
			wp_enqueue_style( 'agile-fe-bootstrap', plugins_url('css/agile-bootstrap.css', __FILE__) );
		}
		
	}// calss ends
}//if ends

new Aspk_Myob_Api_Solution();